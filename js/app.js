//// Global vars
var updateLaterTimer = null;







//// Functions 
function updateMenu(){
    var date = document.getElementById("select-native-4").value;
    //if custom date

    if ( date == "init" ){
        location.reload();
        //main();
    }


    //Always update data
    //use selected place
    var qStr = getSelectedPlace() + date;
    DB.worker.videoDataWorker.getJSON([qStr]);

    //Elements
    var ele = document.getElementById("menuContainer");
    var jsonObj = DB.dat.readMenu(qStr);
    var innerHtmlStr = "";

    //check if we have data
    if ( jsonObj ){
        //For timer if missing data
        if ( typeof (updateLaterTimer) == "number" ){
            clearInterval(updateLaterTimer);
            updateLaterTimer = null;
        }
        ;
        for ( x in jsonObj["courses"] ){
            innerHtmlStr += "<div class='ui-bar ui-bar-a'><h2>";
            if ( jsonObj["courses"][x]["category"] ){
                innerHtmlStr += jsonObj["courses"][x]["category"];
            }
            innerHtmlStr += "</h2></div>";
            //fin
            innerHtmlStr += "<div class='ui-body ui-body-a'><p><b>";
            if ( jsonObj["courses"][x]["title_fi"] ){
                innerHtmlStr += jsonObj["courses"][x]["title_fi"];
            }
            innerHtmlStr += "</b>";
            if ( jsonObj["courses"][x]["properties"] ){
                innerHtmlStr += " "
                innerHtmlStr += jsonObj["courses"][x]["properties"];
            }
            innerHtmlStr += "<br/>";
            if ( jsonObj["courses"][x]["desc_fi"] ){
                innerHtmlStr += jsonObj["courses"][x]["desc_fi"];
            }
            innerHtmlStr += "</p>";
            //eng
            innerHtmlStr += "<p><b>";
            if ( jsonObj["courses"][x]["title_en"] ){
                innerHtmlStr += jsonObj["courses"][x]["title_en"];
            }
            innerHtmlStr += "</b>";
            if ( jsonObj["courses"][x]["properties"] ){
                innerHtmlStr += " "
                innerHtmlStr += jsonObj["courses"][x]["properties"];
            }
            innerHtmlStr += "<br/>";
            if ( jsonObj["courses"][x]["desc_en"] ){
                innerHtmlStr += jsonObj["courses"][x]["desc_en"];
            }
            innerHtmlStr += "</p>";

            //price
            innerHtmlStr += "<p>";
            if ( jsonObj["courses"][x]["price"] ){
                innerHtmlStr += jsonObj["courses"][x]["price"];
                innerHtmlStr += " €"
            }
            innerHtmlStr += "</p></div>";
        }

        if ( innerHtmlStr.length === 0 ){
            innerHtmlStr = "<div class='ui-bar ui-bar-a'><h2>Closed/Unavailable</h2></div><div class='ui-body ui-body-a'><p>No menu available for this day!</p></div>";
        }

        ele.innerHTML = innerHtmlStr;
    }else{
        //no data, come back later
        if ( typeof (updateLaterTimer) != "number" ){
            updateLater();
            ele.innerHTML = "<div class='ui-bar ui-bar-a'><h2>Loading</h2></div><div class='ui-body ui-body-a'><p>Loading content...</p><img id='loadingIcon' src='img/icon_loadB.gif'/></div>";
        }
        ;
    }
}


//Function to update menu when selection changed
function changedDate(s_date){
    if ( s_date == "datepick" ){
        //open date picker
        $("#popupMenu").popup("open");
    }else if ( s_date == "manuallyUpdate" ){
        location.reload();
    }else{
        updateMenu();
    }
}

//cancel pressed on custom date
function customDateCanceled(){
    dateEle = document.getElementById("select-native-4");
    var index = 0;

    dateEle.selectedIndex = index;
    dateEle.value = dateEle.options[index].value
    $('#select-native-4').selectmenu('refresh', true);

    $("#popupMenu").popup("close");
    updateMenu();
}


//run when custom date changed
function customDateChanged(date){
    //stupid US format "12/22/2015" => "2015/12/22"
    var dateStr = document.getElementById("customDate").value;
    var usDate = new Date(dateStr)

    changeFirstOption(usDate);
    $("#popupMenu").popup("close");
}

//change first option
function changeFirstOption(dateObj){
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    //create date string
    var date = "";
    date += dateObj.getFullYear();
    date += "/"
    if ( dateObj.getMonth() + 1 < 10 ){
        date += "0";
    }
    date += dateObj.getMonth() + 1;
    date += "/"
    if ( dateObj.getDate() < 10 ){
        date += "0";
    }
    date += dateObj.getDate();



    var today = new Date();
    var weekStr = date + " " + days[dateObj.getDay()];
    if ( dateObj.toLocaleDateString() == today.toLocaleDateString() ){ //Add current day mark
        weekStr += "*";
    }

    //Write date string to element 
    dateEle = document.getElementById("select-native-4");
    dateEle.options[0].value = date;
    dateEle.options[0].text = weekStr;
    dateEle.selectedIndex = 0;
    dateEle.value = dateEle.options[0].value

    //refresh
    $('#select-native-4').selectmenu('refresh', true);
    updateMenu();
}




//Function to update menu again if failed
function updateLater(){
    if ( typeof (updateLaterTimer) != "number" ){
        //log( 6, "INFO timer set" );
        updateLaterTimer = setInterval(updateMenu, 2000);	//Set delay
    }
    ;
}

//Mote to yesterday
function moveToYesterday(){
    var dateStr = document.getElementById("select-native-4").value;

    document.getElementById("menuContainer").classList.add("animate");
    setTimeout(removeAni, 600);

    var oldDate = new Date(dateStr)
    oldDate.setDate(oldDate.getDate() - 1);

    solveDates(oldDate, -1);
    changeFirstOption(oldDate);
}

//Function to remove animate class from main page
function removeAni(){
    document.getElementById("menuContainer").classList.remove("animate");
    document.getElementById("menuContainer").classList.remove("animateRw");
}






//Mote to tomorrow
function moveToTomorrow(){
    var dateStr = document.getElementById("select-native-4").value;

    document.getElementById("menuContainer").classList.add("animateRw");
    setTimeout(removeAni, 600);

    var oldDate = new Date(dateStr)
    oldDate.setDate(oldDate.getDate() + 1);

    solveDates(oldDate, 1);
    changeFirstOption(oldDate);
}


//Function to solve dates
function solveDates(dateObj, count){
    var dArrow = new Array();  //for quarry
    var d = new Date(dateObj);


    var k = count;
    if ( k < 0 ){
        k *= -1;
    }
    var v = 0;
    for ( var j = 0; j < k; j++ ){
        if ( count < 0 ){ //if negative/positive
            v = count + j;
        }else{
            v = count - j;
        }
        d.setDate(dateObj.getDate() + v);

        //today string
        var newStr = "";
        newStr += d.getFullYear();
        newStr += "/"
        if ( d.getMonth() + 1 < 10 ){
            newStr += "0";
        }
        newStr += d.getMonth() + 1;
        newStr += "/"
        if ( d.getDate() < 10 ){
            newStr += "0";
        }
        newStr += d.getDate();

        //use selected place
        var qStr = getSelectedPlace() + newStr;
        dArrow.push(qStr);
        log(6, "INFO push to list " + newStr);
    }

    DB.worker.videoDataWorker.getJSON(dArrow);
}


//Function to update list of date's
function updateDateList(){
    //today is?
    var d = new Date();
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    //today string
    var newStr = "";
    newStr += d.getFullYear();
    newStr += "/"
    if ( d.getMonth() + 1 < 10 ){
        newStr += "0";
    }
    newStr += d.getMonth() + 1;
    newStr += "/"
    if ( d.getDate() < 10 ){
        newStr += "0";
    }
    newStr += d.getDate();


    //Generate this week
    var optionsStr = ""
    optionsStr += "<option disabled=true id='customOptDate' onclick='$( \"#popupMenu\" ).popup();' value='";
    optionsStr += newStr;
    optionsStr += "'>";
    optionsStr += newStr;
    optionsStr += " ";
    optionsStr += days[d.getDay()];
    optionsStr += "</option>";
    optionsStr += "<option value='datepick'>Datepicker</option>";
    optionsStr += "<optgroup label='This week'>"
    optionsStr += generateWeekOptions(d);
    optionsStr += "</optgroup>"

    var newD = new Date();
    //newD.setDate(d.getDate() + 7);	//Wont always work because new year bug
    newD = setDateFix(newD, 7);
    
    optionsStr += "<optgroup label='Next week'>"
    optionsStr += generateWeekOptions(newD);
    optionsStr += "</optgroup>"

    dateEle = document.getElementById("select-native-4");
    if ( dateEle ){
        dateEle.innerHTML = optionsStr;
        changeFirstOption(d);
    }else{
        log(2, "ERROR: List not updated! page not loaded");
    }
}


//Function to generate <options> list for whole week
function generateWeekOptions(d){
    //var dArrow = new Array() ;  //for quarry
    var weekD = d.getDay();
    //var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    //if 1 generate 0 dates back Mon
    //if 2 generate 1 dates back Tue
    //...
    //if 6 generate 5 dates back Sat
    //if 0 generate 6 dates back Sun


    //Generate this week
    var i = weekD - 1;
    if ( i === -1 ){
        i = 6;
    }
    var newD = new Date();
    var optStr = "";

    for ( ; i > 0; i-- ){ //days back
        var newStr = "";
        //newD.setDate(d.getDate() - i);
        newD = setDateFix(d, -i);

        optStr += "<option value='";
        newStr += newD.getFullYear();
        newStr += "/"
        if ( newD.getMonth() + 1 < 10 ){
            newStr += "0";
        }
        newStr += newD.getMonth() + 1;
        newStr += "/"
        if ( newD.getDate() < 10 ){
            newStr += "0";
        }
        newStr += newD.getDate();
        optStr += newStr;
        optStr += "'>";
        optStr += newStr;
        optStr += " ";
        optStr += days[newD.getDay()];
        optStr += "</option>";

        //dArrow.push(newStr);
    }


    //if 1 generate 6 dates forward Mon
    //if 2 generate 5 dates forward Tue
    //...
    //if 6 generate 1 dates forward Sat
    //if 0 generate 0 dates forward Sun
    i = 7 - weekD;
    if ( i === 7 ){
        i = 0;
    }
    newD = new Date(d);
    for ( var k = 0; k <= i; k++ ){ //days forward
        var newStr = "";
        //newD.setDate(d.getDate() + k);

        var today = new Date();
        if ( newD.toString() == today.toString() ){
            optStr += "<option selected='selected' value='";
        }else{
            optStr += "<option value='";
        }

        newStr += newD.getFullYear();
        newStr += "/"
        if ( newD.getMonth() + 1 < 10 ){
            newStr += "0";
        }
        newStr += newD.getMonth() + 1;
        newStr += "/"
        if ( newD.getDate() < 10 ){
            newStr += "0";
        }
        newStr += newD.getDate();
        optStr += newStr;
        optStr += "'>";
        optStr += newStr;
        optStr += " ";
        optStr += days[newD.getDay()];
        if ( newD.toString() == today.toString() ){ //Add current day mark
            optStr += "*";
        }
        optStr += "</option>";

        //dArrow.push(newStr);

	//Counter measure to random bug when changing year
	newD.setDate(newD.getDate() +1);
    }

    //Lets quarry all
    //DB.worker.videoDataWorker.getJSON(dArrow);

    return optStr;
}



//Fix new year bug in date object
function setDateFix(d, i){
    var direct = 1;
    var count = i;
    var dat = new Date(d);
    if (count < 0){
        direct = -1;
        count = count * -1;
    }
    for ( var k = 0; k < count; k++ ){
        dat.setDate(dat.getDate() + direct);
    }
    return dat;
}


//Get place id to use in querry
function getSelectedPlace(){
    selEle = document.getElementById("selectPlace");
    if ( selEle ){
        return selEle.value + "/";
    }else{
        log(2, "ERROR: List not updated! page not loaded");
        return null;
    }
}

//Set new place name for header
function updatePlaceName(){
    selEle = document.getElementById("selectPlace");
    headEle = document.getElementById("headName");
    if ( selEle && headEle ){
        headEle.innerHTML = selEle.selectedOptions[0].text;
        updateMenu();
    }else{
        log(2, "ERROR: List not updated! page not loaded");
    }
}


//function to read default place from memory and apply it
function setDefaultPlace(){
    var defPlace = DB.dat.loadData("defPlace");
    if ( defPlace ){
        selEle = document.getElementById("selectPlace");
        selEle.selectedIndex = defPlace;
        selEle.value = selEle.options[defPlace].value
        //$('#selectPlace').selectmenu('refresh', true);
    }
}



function pressedKey(oToCheckField, oKeyEvent){
    var a = oKeyEvent.charCode === 0 || /\d/.test(String.fromCharCode(oKeyEvent.charCode));
    //Pressed other then ASCII key
    if ( oKeyEvent.charCode === 0 ){
        // INFO keydata DOM_VK_LEFT  is 37
        // INFO keydata DOM_VK_UP    is 38
        // INFO keydata DOM_VK_RIGHT is 39
        // INFO keydata DOM_VK_DOWN  is 40
        if ( oKeyEvent.keyCode === 37 ){
            moveToYesterday();
        }else if ( oKeyEvent.keyCode === 39 ){
            moveToTomorrow();
        }

    }
    //for ( i in oKeyEvent ) {
    //    log(3, "INFO keydata " +i +" is " +oKeyEvent[i])
    //}
    //alert("Yesss!!!" +oKeyEvent.keyCode);
}











///// Jquerry handlerit

//Swipe to left to go Tomorrow
$(function (){
    // Bind the swipeleftHandler callback function to the swipe event on div.box
    $("#index").on("swipeleft", swipeleftHandler);

    // Callback function references the event target and adds the 'swipeleft' class to it
    function swipeleftHandler(event){
        $(event.target).addClass("swipeleft");
        moveToTomorrow();
    }
});


//Swipe to right to go Yesterday
$(function (){
    // Bind the swipeleftHandler callback function to the swipe event on div.box
    $("#index").on("swiperight", swipeleftHandler);

    // Callback function references the event target and adds the 'swipeleft' class to it
    function swipeleftHandler(event){
        $(event.target).addClass("swiperight");
        moveToYesterday();
    }
});












//// Init()
DB.worker.videoDataWorker.startWorker();

//$.event.special.swipe.scrollSupressionThreshold = 10; 
//$.event.special.swipe.durationThreshold = 1000;
$.event.special.swipe.horizontalDistanceThreshold = 100;
$.event.special.swipe.verticalDistanceThreshold = 60;










//// Main()

function main(){
    //updateMenu("2015/12/10");
    //var jsonObj = DB.dat.readMenu("2015/12/10");
    
    //Rewrite func to receive event
	DB.worker.videoDataWorker.receiveMessageEvent = function (g){
		if ( g[0] === "MENUOBJ" ){
			if ( typeof (updateLaterTimer) == "number" ){
				updateMenu();
			}
		}
	}

    setDefaultPlace();
    updateDateList();
    updatePlaceName();
    solveDates(new Date(), 2);
    solveDates(new Date(), -2);

}

//$(document).bind('pagecreate', main());

