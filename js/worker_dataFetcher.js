// worker_dataFetcher.js
// for geting menu json 

// global var
worker = new Object();
worker.running = false; //-1 = ended 0-inf = working
worker.listOfIds = new Array(); //list of id+dates, format:IIIII/YYYY/MM/DD
worker.timer = null;    //init timer



// function for getting messages
// Start event
self.onmessage = function (event){
    var message = event.data.split(";");
    if ( message[0] ){
        if ( message[0] === "STOP" ){
            stopTimer();
            worker.listOfIds = new Array();
            worker.running = false;
            sendDataBack("STATE", "Stopped");

        }else if ( message[0] === "STATE" ){
            if ( worker.running ){
                sendDataBack("STATE", "Running with " + worker.listOfIds.length + " left");
            }else{
                if ( worker.listOfIds.length > 0 ){
                    sendDataBack("STATE", "Not running");
                }else{
                    sendDataBack("STATE", "Ended");
                }
                ;
            }
            ;

        }else if ( message[0] === "IDLIST" ){
            addToList(message[1]);

        }else{
            log(4, "ERROR message " + message[0] + " type is unnown");
        }
        ;
    }else{
        log(5, "ERROR no messages");
    }
    ;
};


// function to send data back
function sendDataBack(type, data, extraData){
    postMessage(type + ';' + data + ';' + extraData);
}

// logging
function log(level, message){
    sendDataBack("INFO", message, level);
}

// maybe working error management
self.onerror = function (event){
    log(1, event.data);
};

// function to add list of video ids
function addToList(newList){
    var count = 0;
    //newList = removeSpecialChars( newList );  //now we need '/'
    var list = newList.split(',');
    for ( i in list ){
        if ( list[i].length >= 12 && list[i].length <= 16 ){
            worker.listOfIds.push(list[i]);
            count++;
        }else{
            log(5, "WARNING date string " + list[i] + " was not 10 char long");
        }
    }
    ;
    log(5, "INFO added " + count + " date's to get json");
    if ( !worker.running ){
        solveNext();
    }
    ;
}
;

// function to start and reset timer
function resetTimer(){
    if ( worker.timer ){
        stopTimer();
    }
    ;

    worker.timer = setTimeout(function (){
        solveNext();
    }, 200); //time in msec between request
    worker.running = true;
    //log( 6, "INFO worker timer started");

}
;

// function to stop timer
function stopTimer(){
    clearTimeout(worker.timer);
    worker.timer = null;
    worker.running = false;
    //log( 6, "INFO worker timer stopped");
}
;

// function to solve video details
function solveNext(){

    var newId = worker.listOfIds.pop();

    if ( typeof (newId) === "string" && newId.length >= 12 && newId.length <= 16 ){

        xmlhttp = new XMLHttpRequest();

//http://www.sodexo.fi/ruokalistat/output/daily_json/5865/2015/12/11/fi
//http://www.sodexo.fi/ruokalistat/output/weekly_json/5865/2015/12/11/fi
        xmlhttp.open("GET", "../getXML.php?date=" + newId, true);

        xmlhttp.onload = function (e){
            if ( this.status == 200 ){
                //new parse JSON
                //var dataObj = eval('(' + this.response + ')');
                //Remove separator char
                var str = this.response;
                str = str.replace(/\;/g, ",");
                sendDataBack("MENUOBJ", str, newId);

            }
        };

        xmlhttp.onerror = function (event){
            log(1, event.data);
        };

        xmlhttp.send();


    }else{
        log(6, "WARNING date " + newId + " is not valid");
        stopTimer();
    }
    ;



    if ( worker.listOfIds.length > 0 ){
        resetTimer();
    }else{
        stopTimer();
    }
    ;

}
;







function removeAllSpecialChars(str){
    /* checklist
     x type string */

    // remove some stuff
    if ( typeof (str) === "string" ){
        str = str.replace(/\"/g, "");
        str = str.replace(/\'/g, "");
        str = str.replace(/\:/g, " ");
        str = str.replace(/\{/g, "");
        str = str.replace(/\}/g, "");
        str = str.replace(/\[/g, "");
        str = str.replace(/\]/g, "");
    }
    ;
    return str;
}
;

function removeSpecialChars(str){
    /* checklist
     x type string */
    //log( 5, "removeSpecialChars( " + str + " )" );

    // remove some stuff
    if ( typeof (str) === "string" ){
        str = str.replace(/\"/g, "");
        str = str.replace(/\'/g, "");
        str = str.replace(/\:/g, " ");
    }
    ;
    return str;
}
;


