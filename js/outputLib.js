// Library for displaying information

logginLevel = 7;
/*
 1. Critical (always go to user)
 2. Important (may go to user)
 3. Informational (may go to user)
 4. Loggin (not for user)
 5. Debug (never go to user)
 6. VeryDebug (never go to user)
 */
function setLogLevel(level){
    if ( level < 1 ){
        logginLevel = 1;
    }
    ;
    if ( level > 6 ){
        logginLevel = 6;
    }
    ;
}
;

// Console
function log(level, data){
    if ( level <= logginLevel ){
        if ( level === 1 ){
            console.log('%c ' + level + ": " + data, 'color: red;');
        }else if ( level === 2 ){
            console.log('%c ' + level + ": " + data, 'color: darkred;');
        }else if ( level === 3 ){
            console.log('%c ' + level + ": " + data, 'color: blue;');
        }else if ( level === 4 ){
            console.log('%c ' + level + ": " + data, 'color: green;');
        }else if ( level === 5 ){
            console.log('%c ' + level + ": " + data, 'color: black;');
        }else if ( level === 6 ){
            console.log('%c ' + level + ": " + data, 'color: gray;');
        }
        ;
    }
    ;
}
;

// Alert Box
function alertWindow(data){
    log(5, "INFO messageWindow() text=" + data);
    window.alert(data);
}
;

// Prompt Box
function asktextWindow(data, value){
    log(5, "INFO asktextWindow() text=" + data + " default=" + value);
    var rdata = window.prompt(data, value);
    log(6, "get " + rdata);
    return rdata;
}
;

// Confirm Box
function confirmWindow(data){
    log(5, "INFO messageWindow() text=" + data);

    var rdata = window.confirm(data);

    if ( rdata == true ){
        log(6, "INFO get True");
        return true;
    }else{
        log(6, "INFO get False");
        return false;
    }
    ;
}
;

