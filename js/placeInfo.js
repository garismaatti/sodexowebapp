
//function to get new info place
function infoPlaceChanged(thisPlace){
    //Change button label
    setInfoPlaceButton();


    selEle = document.getElementById("info-place");
    conEle = document.getElementById("info-container");

    conEle.innerHTML = "<div class='ui-body ui-body-a'><p>Loading content...</p><img id='loadingIcon' src='img/icon_loadB.gif'/></div>";
    var index = selEle.selectedIndex;
    var placeName = selEle.selectedOptions[0].dataset.href;
    if ( thisPlace ){
        placeName = thisPlace;
    }




    xmlhttp = new XMLHttpRequest();

    xmlhttp.open("GET", "./getPlaceXML.php?place=" + placeName, true);

    xmlhttp.onload = function (e){
        if ( this.status === 200 ){
            var str = this.response;
            //str = str.replace(/\./g, ".<br\/>");
            conEle.innerHTML = str;

        }
    };

    xmlhttp.onerror = function (event){
        log(1, event.data);
    };

    xmlhttp.send();
}


//function to set info place
function setInfoPlace(index){
    if ( index ){
        selEle = document.getElementById("info-place");
        selEle.selectedIndex = index;
        selEle.value = selEle.options[index].value;
        setInfoPlaceButton();
    }
    infoPlaceChanged();
}


function setInfoPlaceButton(){
    selEle = document.getElementById("info-place");
    index = selEle.selectedIndex;
    selBtnEle = document.getElementById("info-place-button");
    if ( selBtnEle ){
        selBtnEle.text = selEle.options[index].text; //Fix showing text
        log(6, "New infoPlaceText=" + index);
    }
}