function getVersion(){
var xmlhttp = new XMLHttpRequest();
var url = "version.php";

xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        parseVersion(xmlhttp.responseText);
    }
}

xmlhttp.open("GET", url, true);
xmlhttp.send();
}

function parseVersion(response) {
    var arr = JSON.parse(response);
    var i;
    var out = "Version: ";

    if (arr  && arr.version){
        out += arr.version;
    }else{
        out += "Unknown";
    }
    out += "";
    document.getElementById("appVersion").innerHTML = out;
}
