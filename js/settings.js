
//All item in memory (localstorage)
function updateMemUsage(){
    document.getElementById("memCount").innerHTML = DB.dat.getStorageSize();
}

/*
 //count only menus
 function updateMemUsage2(){
 var count = 0; 
 var storageSize = DB.dat.getStorageSize() -1;
 var key = "";
 for (var i = storageSize; i >= 0; i--) {
 key = localStorage.key(i);
 if ( key.length === 10 && key[0] === "2" ) {  //2 as in 2015
 count++;
 };
 };
 
 document.getElementById("memCount").innerHTML = count + "/" + storageSize;
 }
 */

function clearMem(){
    //DB.dat.clearAllLocalMenus();  //only menus
    DB.dat.clearAllLocalData();     //All
    updateMemUsage();
}


function clearCache(){
    DB.dat.clearAllLocalMenus();  //only menus
    //DB.dat.clearAllLocalData();     //All
    updateMemUsage();
}


//function to save new default place
function defaultPlaceChanged(){
    selEle = document.getElementById("default-place");
    //save index for now, may need to change later
    DB.dat.saveData("defPlace", selEle.selectedIndex);

}

//function to read default place from memory and apply it
function setDefaultPlace2(){
    var defPlace = DB.dat.loadData("defPlace");
    if ( defPlace ){
        selEle = document.getElementById("default-place");
        selEle.selectedIndex = defPlace;
        selEle.value = selEle.options[defPlace].value
        $('#default-place').selectmenu('refresh', true);

        document.getElementById("memReadStatus").innerHTML = "Read success";
        setTimeout(clearMemReadStatus, 3000);
    }else{
        document.getElementById("memReadStatus").innerHTML = "Nothing in memory";
        setTimeout(clearMemReadStatus, 3000);
    }
}

//function clear the status after reading default place from memory
function clearMemReadStatus(){
    document.getElementById("memReadStatus").innerHTML = "";
}

