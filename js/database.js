// Database API

/* 
 Select best database to use localy
 and only use that.
 
 choices:
 1: IndexedDB			// chrome, FF, IE
 2: Web SQL database 	// chrome, safari, opera, iOS, android
 3: Web Storage			// Backup for all
 */

function removeAllSpecialChars(str){
    /* checklist
     x type string */

    // remove some stuff
    if ( typeof (str) === "string" ){
        str = str.replace(/\"/g, "");
        str = str.replace(/\'/g, "");
        str = str.replace(/\:/g, " ");
        str = str.replace(/\{/g, "");
        str = str.replace(/\}/g, "");
        str = str.replace(/\[/g, "");
        str = str.replace(/\]/g, "");
        str = str.replace(/\//g, "");
        str = str.replace(/\\/g, "");
    }
    ;
    return str;
}
;

function removeSpecialChars(str){
    /* checklist
     x type string */
    //log( 5, "removeSpecialChars( " + str + " )" );

    // remove some stuff
    if ( typeof (str) === "string" ){
        str = str.replace(/\"/g, "");
        str = str.replace(/\'/g, "");
        str = str.replace(/\//g, "");
        str = str.replace(/\\/g, "");
        str = str.replace(/\:/g, " ");
    }
    ;
    return str;
}
;


function fixJavaEncoding(str){

    // remove some stuff
    if ( typeof (str) === "string" ){
        str = str.replace(/\\u00c4/g, "Ä");
        str = str.replace(/\\u00e4/g, "ä");
        str = str.replace(/\\u00d6/g, "Ö");
        str = str.replace(/\\u00f6/g, "ö");
        str = str.replace(/\\u00c5/g, "Å");
        str = str.replace(/\\u00e5/g, "å");
        str = str.replace(/\;/g, ",");
        str = str.replace(/\\\//g, "/");
    }
    ;
    return str;
}
;



//////  SETTINGS OBJECT //////
function settingsObj(){
    //Add settings here

    // [ saved ] //
    if ( typeof (Storage) !== "undefined" ){
        this.databaseIs = "webstorage"; /*"websqldb";*/ /*"indexeddb";*/
    }else if ( true ){
        this.databaseIs = "null";
    }

    this.curDefPlaceIndex = 0;

}
settings = new settingsObj();








//////  DATABASE OBJECT //////
var DB = new Object();
DB.dat = new Object();
DB.worker = new Object();
DB.manage = new Object();



//////  Database data functions //////
/// DB.dat.lastDataChange = "";

DB.dat.saveData = function (id, data){
    if ( settings.databaseIs === "webstorage" ){
        localStorage.setItem(id, data);
    }else{
        log(1, "FATAL ERROR! Can't save to database. database not set.");
    }
    ;
};



DB.dat.loadData = function (id){
    var returnData = null;
    if ( settings.databaseIs === "webstorage" ){
        returnData = localStorage.getItem(id);
    }else{
        log(1, "FATAL ERROR! Can't load from database. database not set.");
    }
    ;

    return returnData;
};



DB.dat.removeData = function (id){
    if ( settings.databaseIs === "webstorage" ){
        localStorage.removeItem(id);
    }else{
        log(1, "FATAL ERROR! Can't remove data from database. database not set.");
    }
    ;
};



DB.dat.clearAllLocalData = function (){
    if ( settings.databaseIs === "webstorage" ){
        localStorage.clear();
        log(2, "WARNING Database was cleared!");
    }else{
        log(1, "FATAL ERROR! Can't clear database. database not set.");
    }
    ;
};




DB.dat.clearAllLocalVideoData = function (){
    if ( settings.databaseIs === "webstorage" ){
        var storageSize = localStorage.length - 1;
        var key = "";
        for ( var i = storageSize; i >= 0; i-- ){
            key = localStorage.key(i);
            if ( key.length === 11 ){
                DB.dat.removeData(key);
            }
            ;
        }
        ;
    }else{
        log(1, "FATAL ERROR! Can't clear video data from database. database not set.");
    }
    ;
};



DB.dat.getStorageSize = function (){
    if ( settings.databaseIs === "webstorage" ){
        return localStorage.length;
    }else{
        log(1, "FATAL ERROR! Can't get database size. database not set.");
    }
    ;
};


DB.dat.clearAllLocalMenus = function (){
    if ( settings.databaseIs === "webstorage" ){
        var storageSize = localStorage.length - 1;
        var key = "";
        for ( var i = storageSize; i >= 0; i-- ){
            key = localStorage.key(i);
            if ( key.length >= 12 && key.length <= 16 ){  //no better for now
                DB.dat.removeData(key);
                log(6, "INFO: removed menu=" + key);
            }
            ;
        }
        ;
    }else{
        log(1, "FATAL ERROR! Can't clear menu data from database. database not set.");
    }
    ;
};


DB.dat.readMenu = function (date){
    if ( settings.databaseIs === "webstorage" ){
        var data = localStorage.getItem(date);
        if ( typeof (data) !== "undefined" ){
            var JSONObject = JSON.parse(data);
            return JSONObject;
        }
    }else{
        log(1, "FATAL ERROR! Can't clear menu data from database. database not set.");
    }
    ;
    return null;
};



/* // later
 DB.dat.exportPlaylist = function (type);
 DB.dat.importPlaylist = function (type, data);
 DB.dat.sendData = function (id, data);
 DB.dat.reciveData = function (id, data);
 */


//////  Database management functions //////



//////  Database worker functions //////
DB.worker.videoDataWorker = new Object();
DB.worker.videoDataWorker.itself = null;

DB.worker.videoDataWorker.startWorker = function (){

    if ( Worker ){
        if ( !DB.worker.videoDataWorker.itself ){
            DB.worker.videoDataWorker.itself = new Worker("js/worker_dataFetcher.js");
            log(6, "INFO video data worker started");
        }else{
            DB.worker.videoDataWorker.stopWorker();
            DB.worker.videoDataWorker.itself = new Worker("js/worker_dataFetcher.js");
            log(5, "INFO video data worker restarted");
        }
        ;
    }else{
        log(1, "ERROR Your browser does not support Web Workers");
    }
    ;

    DB.worker.videoDataWorker.itself.onmessage = function (event){
        DB.worker.videoDataWorker.receiveMessage(event);
    };
};
DB.worker.videoDataWorker.startWorker();

DB.worker.videoDataWorker.sendMessage = function (command, data){
    /* COMANDS to send
     - IDLIST
     - STOP
     - STATE
     */
    DB.worker.videoDataWorker.itself.postMessage(command + ";" + data);
    log(6, "INFO Send command " + command + " to videoDataWorker");
};

DB.worker.videoDataWorker.getJSON = function (needToSolve){
    if ( DB.worker.videoDataWorker.itself ){
        DB.worker.videoDataWorker.sendMessage("IDLIST", needToSolve);
    }else{
        log(2, "ERROR no videoDataWorker");
    }
    ;
};


DB.worker.videoDataWorker.state = function (){
    if ( DB.worker.videoDataWorker.itself ){
        DB.worker.videoDataWorker.sendMessage("STATE");
    }else{
        log(2, "ERROR no videoDataWorker");
    }
    ;
};


DB.worker.videoDataWorker.receiveMessage = function (event){
    var g = event.data.split(';');

    if ( g[0] ){
        if ( g[0] === "MENUOBJ" ){
            log(6, "INFO get JSON from worker: " + g[1]);
            var tmpStr = fixJavaEncoding(g[1]);
            var JSONObject = JSON.parse(tmpStr);
            DB.dat.saveData(g[2], tmpStr);    //save as "2015/12/23"
            log(6, "INFO saved JSON " + g[2]);

        }else if ( g[0] === "STATE" ){
            log(2, 'INFO worker state is: ' + g[1]);
        }else{
            // if unnown return value
            log(5, g[0] + '; ' + g[1]);
        }
        DB.worker.videoDataWorker.receiveMessageEvent(g);	//send event
    }
    ;
};

//function to be overwrited in code to get receive-event
DB.worker.videoDataWorker.receiveMessageEvent = function (event){
	var a = 0;
}

// Function for stopping worker
DB.worker.videoDataWorker.stopWorker = function (){
    if ( Worker ){
        DB.worker.videoDataWorker.itself.terminate();
        DB.worker.videoDataWorker.itself = null;
        log(5, "INFO Stopped video data worker");
    }else{
        log(1, "ERROR Your browser does not support Web Workers");
    }
    ;
};


