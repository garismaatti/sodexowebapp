<?php

// This will take ?place="/aaltoyliopiston_kahvila from url
// then download html page from server
// then parse part of page
// then print and save that part of page as xml
// the if ask again print xml from file

header('Content-type: text/html; charset=UTF-8');


//Get place string
//place="/aaltoyliopiston_kahvila"
$place = htmlspecialchars($_GET["place"]);

if (strlen($place) > 60) {  //if to big
    echo "Don't try to exploit my system!";
    exit();
} else if (strlen($place) < 3) {  //if to small
    echo "I created this empty page just for you.";
} else {    //if input is okay
    //file name
    $name = str_replace("/", "", $place);

    //Automatic removal if too old
    if (file_exists("savedPlaces/$name.xml")) {
        $fileDate = date("Ymd", filectime("savedPlaces/$name.xml"));
        $now = date("Ymd");
        //date limit
        if ($now - $fileDate >= 1) {
            //remove xml file
            unlink("savedPlaces/$name.xml");
        }
    }

    //read from file if exist
    if (file_exists("savedPlaces/$name.xml")) {
        $myfile = fopen("savedPlaces/$name.xml", "r") or die("Unable to open file!");
        echo fread($myfile, filesize("savedPlaces/$name.xml"));
        fclose($myfile);
        //echo "<div>Cached on: ".date("Y-m-d H:i:s.",filectime("savedPlaces/$name.xml"))."</div>";
    } else if ("$name" == "hermia-6-belounasravintola") {
        echo "<pre>No info available</pre>";
    } else if ("$name" == "lounas") {
        echo "<pre>No info available</pre>";
    } else if ("$name" == "ravintola-bepop") {
        echo "<pre>No info available</pre>";
    } else if ("$name" == "ravintola-terra") {
        echo "<pre>No info available</pre>";
    } else {

        //Get HTML file from source
        //http://www.sodexo.fi/aaltoyliopiston_kahvila
        $content = file_get_contents("http://www.sodexo.fi" . $place);

        //check that is valid HTML
        $notEnt = strpbrk("$content", "Sivulle ei ole pääsyä");
        $notHere = strpbrk("$content", "Sivua ei löytynyt");
        $notExt = "$notEnt$notHere";
        //notExt == "" when both did't happen
        if (strlen($notExt) > 0) {

            //Save to file
            $fp = fopen("savedPlaces/$name", "w");
            fwrite($fp, $content);
            fclose($fp);

            //Get start row number
            $output = shell_exec("grep -n 'block block-sxo-opening-hours block-facility-opening-hours block-sxo-opening-hours-facility-opening-hours odd block-without-title' savedPlaces/$name | awk '{print $1}'");
            $LNNUM = str_replace(":", "", $output);
            $LNNUMB = $LNNUM + 2;
            //echo "<pre>$LNNUMB</pre>";


            if ($LNNUM == "") {
                echo "<pre>Failed to find HEAD</pre>";
            } else {
                //Get end row number
                $ELNUMTMP = shell_exec("grep -n '<span class=\"email\">' savedPlaces/$name | awk '{print $1}'");
                $ELNUM = str_replace(":", "", $ELNUMTMP);
                $ELNUMB = $ELNUM + 1;
                if ($ELNUMTMP == "") {
                    //Second try to get end row number
                    $ELNUMTMP = shell_exec("grep -n 'Ravintolasivut - Healthwise' savedPlaces/$name | awk '{print $1}'");
                    $ELNUM = str_replace(":", "", $ELNUMTMP);
                    $ELNUMB = $ELNUM - 2;
                }
                //echo "ELNUMB = $ELNUMB";

                if ($ELNUMB == "") {
                    echo "<pre>Failed to find TAIL</pre>";
                } else {
                    $LNUM = $ELNUMB - $LNNUMB;
                    //echo "lines=$LNUM";

                    $LNNUMC = trim($LNNUMB); //has '\n' for some reason
                    $D = "d";
                    $STR = "1,$LNNUMC$D";

                    $output = shell_exec("sed \"$STR\" savedPlaces/$name | head -n $LNUM > savedPlaces/$name.xml");
                    //$output = shell_exec( "sed '$STR' 'savedPlaces/$name' 2>&1");
                    //echo "<pre>This is it = $output</pre>";

                    $myfile = fopen("savedPlaces/$name.xml", "r") or die("Unable to open file!");
                    $myContent = fread($myfile, filesize("savedPlaces/$name.xml"));
                    //echo strip_tags($myContent);
                    echo $myContent;
                    fclose($myfile);
                    //echo "<div>Cached on: ".date("Y-m-d H:i:s.",filectime("savedPlaces/$name.xml"))."</div>";
                    //remove full html page
                    unlink("savedPlaces/$name");
                }
            }
        } else {
            echo '<div><p>There were server error while getting data from sodexo<br/>Please try againg after a while.<br/>(If this continues, please report it as bug. The contact info is in about page)</p></div>';
        }
    }//end if file exist
}
?>
