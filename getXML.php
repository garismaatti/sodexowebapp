<?php

// This will take ?date=2015/12/11 from url
// then download json file from server
// then print and save that json
// the if ask again print json from file

header('Content-type: text/html; charset=UTF-8');


//Get date string
//date="2015/12/11"
$date = htmlspecialchars($_GET["date"]);

if (strlen($date) > 16) {  //if to big
    echo "Don't try to exploit my system you bastard!";
    exit();
} else if (strlen($date) < 12) {  //if to small
    echo "I created this empty page just for you.";
} else {    //if input is okay
    //file name
    $name = str_replace("/", "-", $date);
    
    //Automatic removal if too old
	if (file_exists("savedJson/$name.xml")) {
		$fileDate = date("Ymd",filectime("savedJson/$name.xml"));
		$now = date("Ymd");
		//date limit
		if ($now - $fileDate >= 1 ){
			//remove xml file
			unlink("savedJson/$name.xml");
		}
	}

    //read from file if exist
    if (file_exists("savedJson/$name.xml")) {
        //echo readfile("savedJson/$name.xml"); //don't use will print bytes-read at end!
        $myfile = fopen("savedJson/$name.xml", "r") or die("Unable to open file!");
        echo fread($myfile, filesize("savedJson/$name.xml"));
        fclose($myfile);
    } else {

        //Get xml file from source
        //http://www.sodexo.fi/ruokalistat/output/daily_json/5865/2015/12/11/fi
        //http://www.sodexo.fi/ruokalistat/output/weekly_json/5865/2015/12/11/fi
        $content = file_get_contents("http://www.sodexo.fi/ruokalistat/output/daily_json/" . $date . "/fi");

        //check that is valid JSON
        if ($content[0] == '{') {

            echo $content;  //print JSON
            //Save to file
            $fp = fopen("savedJson/$name.xml", "w");
            fwrite($fp, $content);
            fclose($fp);
        } else {
            echo '{"meta":[],"courses":[{"title_en":"Error loading JSON from Sodexo!","category":"Error","desc_en":"This problem should fix it self soon."}]}';
        }
    }//end if file exist
}
?>
