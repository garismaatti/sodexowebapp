# README #

WepApp to use [Sodexo](http://www.sodexo.fi) lunch menus in mobile phones.

Made as part of [JAMK University of Applied Sciences](http://www.jamk.fi/)  Programming of Web-applications -course in 2015.

App home page at [https://bitbucket.org/garismaatti/sodexowebapp/](https://bitbucket.org/garismaatti/sodexowebapp/)



#  #


Sodexo provides open-data-API to get they lunch menus in HTML/XML/JSON.
This app uses JSON.

App uses PHP server to overcome cross-domain reference and to cache JSON strings into server hard disk to lesser provides load.
The app itself runs on apathe.

See working app in address [http://student.labranet.jamk.fi/~K2362/ruokalistaApp/](http://goo.gl/jUngao)
 (Will stop working at some  point)


##How to setup server side:##
1. Download and put git directory under public www directory (usually "/var/www/")
3. Start Apathe and PHP server if not already running
4. Test that php is running by accessing url "http://yourserver/SodexoWebApp/getXML.php?date=5868/2015/12/18" and check that it gives you JSON string
5. Browse to url "http://yourserver/SodexoWebApp/" to use



##How to create a bookmark on android:##
See [this page](http://www.wikihow.com/Set-a-Bookmark-Shortcut-in-Your-Home-Screen-on-Android) how to add bookmark on home screen in android.




##Legal Notice:##
Most of the content and name 'Sodexo' is owned by (c) 2012. Sodexo Oy. 

Sodexo state that data is **free to use as long as it is nonprofitable**.

(so no ads or payment in/of this app!) 

For full statement see [http://www.sodexo.fi/kayttoehdot](http://www.sodexo.fi/kayttoehdot) (written in Finnish).



App has Google(tm) Analytics code included in main page. This code is included to count visitors,
 but it also collects other information of user. More about how Google(tm) handles this information can be read on [Google's privacy policies](http://www.google.com/intl/en/policies/privacy/).

This code should be deleted or replaced by your own.


App itself owned by Copyright (cc 4.0) 2015. Ari Salopää


[http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/) 
![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
Sodexo WebApp
 by 
[Ari Salopää](https://bitbucket.org/garismaatti/sodexowebapp)
 is licensed under a  [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)
Permissions beyond the scope of this license may be available at 
[http://www.sodexo.fi/kayttoehdot](http://www.sodexo.fi/kayttoehdot).


#

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR

IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,

FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE

AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER

LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,

OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN

THE SOFTWARE.